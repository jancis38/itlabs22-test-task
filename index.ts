import path from 'path';
import express, { Request, Response } from 'express';
import bodyParser from 'body-parser';
import { config } from './config';
import { views } from './http/views';

const app: express.Application = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, '../resources/views'));

app.use('/', views);

app.listen(config.PORT, () => {
  console.log(`app running @ localhost:${config.PORT}`);
});
