# Table generator

Generate table(s) by filling/editing form data.

## Installation

Install the necessary packages, before launching the project.

```bash
npm install
```

## Launch project

```bash
npm run tsc
```
```bash
npm run watch
```
```bash
npm run client
```
```bash
npm run start
```