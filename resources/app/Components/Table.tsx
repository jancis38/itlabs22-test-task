import * as React from "react";

export const Table = ({ data, setTable, editRow }: {
    data: any,
    setTable(data: any): void,
    editRow(table: number, row: number): void
}) => {

    const copy = (index: number) => {
        let tableToCopy = [...data[index]];
        let newData = [...data, tableToCopy];
        setTable(newData);
    }

    const remove = (index: number) => {
        let newData = [
            ...data.slice(0, index),
            ...data.slice(index + 1)
        ];
        setTable(newData);
    }

    const rowRemove = (table: number, row: number) => {
        let newData = [
            ...data.slice(0, table),
            [...data[table].slice(0, row),
            ...data[table].slice(row + 1)],
            ...data.slice(table + 1)
        ];

        newData = newData.filter((table, index) => {
            return index === 0 || table.length
        });

        setTable(newData);
    }

    return (
        data.map((row: any, table_index: number) => (
            data[table_index].length > 0 ? (
                <div key={table_index}>
                    <div className="table-controls">
                        <div onClick={() => copy(table_index)} className="btn small">
                            Copy table
                        </div>
                        {data.length > 1 ? (
                            <span style={{ width: '24px' }}>
                                <div onClick={() => remove(table_index)} className="remove-btn"></div>
                            </span>
                        ) : null}
                    </div>

                    <div className="table-border">
                        <table>
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Surname</th>
                                    <th>Age</th>
                                    <th>City</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                {row.map((data: any, row_index: number) => (
                                    <tr key={row_index}>
                                        <td>{data.name}</td>
                                        <td>{data.surname}</td>
                                        <td>{data.age}</td>
                                        <td>{data.city}</td>
                                        <td className="table-row-controls">
                                            <span onClick={() => editRow(table_index, row_index)} className="edit">
                                                Edit
                                            </span>
                                            <span onClick={() => rowRemove(table_index, row_index)} className="delete">
                                                Delete
                                            </span>
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>
                </div>
            ) : null
        ))
    )
};