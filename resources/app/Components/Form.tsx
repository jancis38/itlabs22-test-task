import * as React from "react";

const initialData = {
    name: '',
    surname: '',
    age: 0,
    city: ''
}

export const Form = ({ table, edit, resetEdit, setTable }: {
    table: any,
    edit: any,
    resetEdit(): void,
    setTable(data: any): void
}) => {

    const editState = edit.table_index !== null && edit.row_index !== null ? true : false;
    const [data, setData] = React.useState<FormFields>(initialData);
    const [errors, setErrors] = React.useState<(keyof FormFields)[]>([]);
    const validate = Object.keys(data).filter(
        k => !data[k as keyof FormFields]
    ) as (keyof FormFields)[];

    React.useEffect(() => {
        setData(editState ? table[edit.table_index][edit.row_index] : initialData);
    }, [editState]);

    const submit = () => {
        if (validate.length) {
            setErrors(validate);
            return;
        }

        let newTableData = [[...table[0], data], ...table.slice(1)];
        setTable([...newTableData]);
        setData(initialData);
    }

    const saveRow = () => {
        if (validate.length) {
            setErrors(validate);
            return;
        }

        const table_index = edit.table_index!;
        const row_index = edit.row_index!;

        const newTableData = [
            ...table.slice(0, table_index),
            [...table[table_index].slice(0, row_index),
                data,
            ...table[table_index].slice(row_index + 1)],
            ...table.slice(table_index + 1)
        ];

        setTable([...newTableData]);
        setData(initialData);
        resetEdit();
    }

    const editForm = (field: string, value: string) => {
        if (field === 'age') {
            const regEx = /^\d+$/.test(value);

            if (!regEx) {
                return;
            }
        }

        setErrors(errors.filter(f => f !== field));
        setData({ ...data, [field]: value });
    }

    return (
        <div id="form">
            {errors.length ? <div className="error-message">Fill in all fields!</div> : null}
            <input
                className={errors.indexOf('name') !== -1 ? 'error' : ''}
                placeholder="Name"
                onChange={e => editForm('name', e.target.value)}
                value={data.name}
            />
            <input
                className={errors.indexOf('surname') !== -1 ? 'error' : ''}
                placeholder="Surname"
                onChange={e => editForm('surname', e.target.value)}
                value={data.surname}
            />
            <input
                className={errors.indexOf('age') !== -1 ? 'error' : ''}
                placeholder="Age"
                onChange={e => editForm('age', e.target.value)}
                value={data.age ? data.age : ''}
            />
            <input
                className={errors.indexOf('city') !== -1 ? 'error' : ''}
                placeholder="City"
                onChange={e => editForm('city', e.target.value)}
                value={data.city}
            />

            <div className="btn" onClick={editState ? saveRow : submit} style={{ marginTop: '18px' }}>
                {editState ? 'EDIT' : 'ADD'}
            </div>
        </div>
    )
};