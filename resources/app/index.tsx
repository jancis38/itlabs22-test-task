import * as React from "react";
import * as ReactDOM from "react-dom";
import { Form } from "./Components/Form";
import { Table } from "./Components/Table";

const initialEditState = {
  table_index: null,
  row_index: null
}

const App = () => {
  const [tableData, setTableData] = React.useState<any>([[]]);
  const [editRow, setEditRow] = React.useState<EditRow>(initialEditState);

  return (
    <>
      <Form
        table={tableData}
        edit={editRow}
        resetEdit={() => setEditRow(initialEditState)}
        setTable={(data) => setTableData(data)}
      />
      <Table
        data={tableData}
        setTable={(data) => setTableData(data)}
        editRow={(table, row) => setEditRow({ table_index: table, row_index: row })}
      />
    </>
  )
};

ReactDOM.render(<App />, document.getElementById("app"));
