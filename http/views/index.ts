import path from 'path';
import express from 'express';
const { Router } = express;

export const views: express.Router = Router();

views.use('/css', express.static(path.join(__dirname, '../../../resources/css')));
views.use('/img', express.static(path.join(__dirname, '../../../resources/img')));
views.use('/js', express.static(path.join(__dirname, '../../../resources/js')));

views.get('*', (req, res) => {
  res.render('index');
});
