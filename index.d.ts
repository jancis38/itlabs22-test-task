type FormFields = {
    name: string;
    surname: string;
    age: number;
    city: string;
};

type EditRow = {
    table_index: null | number,
    row_index: null | number,
};